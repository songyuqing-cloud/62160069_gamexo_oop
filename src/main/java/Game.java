
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author BmWz
 */
public class Game {

    Player playerX;
    Player playerO;
    Player turn;
    Table table;
    int row, col;
    String choose;
    Scanner kb = new Scanner(System.in);

    public Game() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void newGame() {
        playerX = new Player('X');
        playerO = new Player('O');
        table = new Table(playerX, playerO);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        table.showTable();
    }

    public void input() {
        while (true) {
            System.out.println("Please input Row Col :");
            row = kb.nextInt() - 1;
            col = kb.nextInt() - 1;
            if (table.setRowCol(row, col)) {
                break;
            }
            System.out.println("Table at row and col is not empty !!");
        }
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " Turn");
    }

    static void showBye() {
        System.out.println("Bye bye ....");
    }

    public void run() {
        this.showWelcome();
        while (true) {
            this.showTable();
            showTurn();
            this.input();
            table.checkWin();
            if (table.isFinish()) {
                this.showTable();
                if (table.getWinner() == null) {
                    System.out.println("Draw");
                } else {
                    System.out.println(table.getWinner().getName() + " Win!!");
                }
                while (true) {
                    System.out.println("Play again (Yes/No): ");
                    choose = kb.next();
                    if (choose.equals("Yes")) {
                        newGame();
                        break;
                    } else if (choose.equals("No")) {
                        showBye();
                        break;
                    } else {
                        System.out.println("--- Input mismatch ---");
                    }
                }
                if (choose.equals("No")) {
                    break;
                }
            }
            table.switchPlayer();
        }

    }
}
